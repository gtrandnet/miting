(function($) {
    "use strict";

    /*----------------------
    Document-Reader-Function
    -----------------------*/
    $(document).on('ready', function() {

        /*--------------------
        Show-Body
        ----------------------*/
        $('body').css({
            'opacity': '1'
        });




        /*----------------------
        Smoth-Scroll-JS
        -----------------------*/
        $('.mainmenu-area a[href*="#"]')
            .not('[href="#"]')
            .not('[href="#0"]')
            .on('click', function(event) {
                // On-page links
                if (
                    location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') &&
                    location.hostname == this.hostname
                ) {
                    // Figure out element to scroll to
                    var target = $(this.hash);
                    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                    // Does a scroll target exist?
                    if (target.length) {
                        // Only prevent default if animation is actually gonna happen
                        event.preventDefault();
                        $('html, body').animate({
                            scrollTop: target.offset().top
                        }, 1000, function() {
                            // Callback after animation
                            // Must change focus!
                            var $target = $(target);
                            $target.focus();
                            if ($target.is(":focus")) { // Checking if the target was focused
                                return false;
                            } else {
                                $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
                                $target.focus(); // Set focus again
                            };
                        });
                    }
                }
            });

        /*--------------------
        Header-Title-Set
        ----------------------*/
        $("h1,h2,h3,h4,h5,h6").each(function() {
            var title_val = $(this).text();
            $(this).attr('title', title_val);
        });

        /*---------------------
        Click-Bubble-JS
        ----------------------*/




        /*-- Drop-Down-Menu--*/
        function dropdown_menu() {
            $('.hamburger .mainmenu').fadeOut();
            var sub_menu = $('.mainmenu .sub-menu'),
                menu_a = $('.mainmenu ul li a');
            sub_menu.siblings('a').append('<i class="fa fa-angle-right"></i>')
            sub_menu.hide();
            sub_menu.siblings('a').on('click', function(event) {
                event.preventDefault();
                $(this).parent('li').siblings('li').find('.sub-menu').slideUp();
                $(this).siblings('.sub-menu').find('.sub-menu').slideUp();
                $(this).siblings('.sub-menu').slideToggle();
                $(this).parents('li').siblings('li').removeClass('open');
                $(this).siblings('.sub-menu').find('li.open').removeClass('open');
                $(this).parent('li').toggleClass('open');
                return false;
            });
        }
        dropdown_menu();


        /*-- Burger-Menu --*/
        function burger_menu() {
            var burger = $('.burger'),
                hm_menu = $('.hamburger .mainmenu');

            burger.on('click', function() {
                $(this).toggleClass('play');
                $(this).siblings('.mainmenu').fadeToggle();
            });

        }
        burger_menu();



        /*---------------------
        Mail-Chimp-Integration
        -----------------------*/



    });

    /*------------------------
    Window-Load-Function
    -------------------------*/
    $(window).on("load", function() {

        /*--------------------
        Preloader-JS
        ---------------------*/
        $('.preloader').fadeOut(500);




    });

})(jQuery);