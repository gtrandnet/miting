<?php

use Illuminate\Http\Request;

Route::post('/register', 'Auth\AuthController@register');
Route::post('/login', 'Auth\AuthController@login');
Route::post('/logout', 'Auth\AuthController@logout');

Route::group(['middleware' => 'jwt.auth'], function(){
    Route::get('/me', 'Auth\AuthController@user');
    Route::post('/destroy/{id}', 'Crm\Room\RoomController@destroy');
    Route::post('/create', 'Crm\Room\RoomController@store');
    Route::post('/update/{room}', 'Crm\Room\RoomController@update');
    Route::get('/get-room/{id}', 'Crm\Room\RoomController@getRoom');
    Route::get('/rooms', 'Crm\Room\RoomController@index');
});


