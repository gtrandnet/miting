<?php

namespace App\Crm\Room;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $fillable = ['title'];
}
