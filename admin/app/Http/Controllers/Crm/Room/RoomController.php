<?php

namespace App\Http\Controllers\Crm\Room;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\RoomRequest;
use App\Crm\Room\Room;
use Carbon\Carbon;
use DB;

class RoomController extends Controller
{
    public function index()
    {
        $room = Room::all();

        return response()->json($room, 200);
    }
    

    public function getStatisticNewOrders()
    {
        $orders = Order::all()
                ->groupBy(function($item)
                { 
                    return $item->created_at->format('d-M-y'); 
                })->toArray();
        foreach ($orders as $key => $value) {
            $data[] = count($value);
            $labels[] = $key;
        }

        return [
            'labels' => $labels,
            'datasets' => [
                [
                'label' => 'Статистика новых заказов!',
                'backgroundColor' => 'green',
                'data' => $data,
                ],
                [
                    'label' => 'Статистика новых заказов!',
                    'backgroundColor' => 'blue',
                    'data' => [1, 45, 23, 57],
                ]
            ]
        ];
    }


    public function changeOrderStatus(Request $request)
    {
        $order = Order::findOrFail($request->id);
        $order->status = $request->status;
        $order->save();
        return response()->json("Success", 200);
    }


    public function destroy($id)
    {
        $room = Room::findOrFail($id);
        $room->delete();
        return response()->json("Комната успешно удалена!", 200);
    }

    public function getRoom($id)
    {
        $room = Room::findOrFail($id);
        return response()->json($room, 200);
    }

    public function store(RoomRequest $request)
    {
        $room = Room::create([
            'title' => $request->title,
        ]);

        return response()->json("Комната успешно создано!", 200);
    }

    public function update(RoomRequest $request, Room $room)
    {
        $room->update([
            'title' => $request->title
        ]);

        return response()->json("Комната успешно изменено!", 200);
    }

}
