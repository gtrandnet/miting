import router from './router'
import store from './vuex'
import localforage from 'localforage'

localforage.config({
    driver: localforage.LOCALSTORAGE,
    storeName: 'miting'
})


require('./bootstrap');

window.Vue = require('vue');



Vue.component('app', require('./components/App.vue').default);
Vue.component('crm-home', require('./app/crm/components/CrmHome.vue').default);
Vue.component('crm-navbar', require('./app/crm/components/CrmNavbar.vue').default);
Vue.component('crm-header', require('./app/crm/components/CrmHeader.vue').default);
Vue.component('navigation', require('./components/Navigation.vue').default);

store.dispatch('auth/setToken').then(() => {
    store.dispatch('auth/fetchUser').catch(() => {
        store.dispatch('auth/clearAuth')
        router.replace({ name: 'login' })
    })
}).catch(() => {
    store.dispatch('auth/clearAuth')
})


const app = new Vue({
    el: '#app',
    router: router,
    store: store,
});