export const rooms = ({ dispatch }, { context }) => {
    return axios.get('/api/rooms').then((response) => {
        context.rooms = response.data
    }).catch((error) => {
        context.errors = error.response.data.errors
    })
}
export const create_room = ({ dispatch }, { payload, context }) => {
    return axios.post('/api/create/', payload).then((response) => {
        context.message = response.data
    }).catch((error) => {
        context.errors = error.response.data.errors
        context.e = true
    })
}
export const update_room = ({ dispatch }, { payload, context }) => {
    return axios.post(`/api/update/${payload.id}`, payload).then((response) => {
        context.message = response.data
    }).catch((error) => {
        context.errors = error.response.data.errors
        context.e = true
    })
}
export const destroy_room = ({ dispatch }, { payload, context }) => {
    return axios.post(`/api/destroy/${payload.id}`).then((response) => {
        context.message = response.data
    }).catch((error) => {
        context.errors = error.response.data.errors
    })
}
export const get_room = ({ dispatch }, { payload, context }) => {
    return axios.get(`/api/get-room/${payload.id}`).then((response) => {
        context.room = response.data
    }).catch((error) => {
        context.errors = error.response.data.errors
    })
}
export const changeOrderStatus = ({ dispatch }, { payload, context }) => {
    return axios.patch('/api/order/change-order-status', payload).then((response) => {
        context.message = response.data
    }).catch((error) => {
        //context.errors = error.data
    })
}