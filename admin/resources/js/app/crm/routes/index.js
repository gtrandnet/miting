import { CrmHome } from '../components';
import { Rooms } from '../components';
import { RoomModel } from '../components';

export default [{
        path: '/',
        components: { main: CrmHome },
        name: 'crm_home',
        redirect: { name: 'rooms' },
        children: [{
                path: '/rooms',
                components: { crm: Rooms },
                name: 'rooms',
                meta: {
                    needsAuth: true
                }
            },
            {
                path: '/create',
                components: { room_model: RoomModel },
                name: 'room_create'
            },
            {
                path: '/update/:id',
                components: { room_model: RoomModel },
                name: 'room_update'
            }
        ],
        meta: {
            needsAuth: true
        }
    },

]