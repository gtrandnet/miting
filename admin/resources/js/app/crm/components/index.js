import Vue from 'vue'

export const CrmHome = Vue.component('crm-home-page', require('./CrmHome.vue').default)
export const Rooms = Vue.component('rooms', require('./room/Rooms.vue').default)
export const RoomModel = Vue.component('room-model', require('./room/RoomModel.vue').default)
export const StatisticNewOrders = Vue.component('statistic-new-orders', require('./room/StatisticNewOrders.vue').default)