import router from './router'
import store from './vuex'
import localforage from 'localforage'


localforage.config({
    driver: localforage.LOCALSTORAGE,
    storeName: 'miting'
})


require('./bootstrap')

window.Vue = require('vue')



Vue.component('app-page', require('./components/App.vue').default)
Vue.component('home-page', require('./app/home/components/Home.vue').default)
Vue.component('empty-component', require('./app/home/components/Empty.vue').default)
Vue.component('error-component', require('./app/home/components/Errors.vue').default)
Vue.component('navigation', require('./components/Navigation.vue').default)

store.dispatch('auth/setToken').then(() => {
    store.dispatch('auth/fetchUser').catch(() => {
        store.dispatch('auth/clearAuth')
        router.replace({ name: 'login' })
    })
}).catch(() => {
    store.dispatch('auth/clearAuth')
})

const app = new Vue({
    el: '#app',
    router: router,
    store: store,
});