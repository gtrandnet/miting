import { NotFound } from '../components'

export default [{
    path: '*',
    components: { main: NotFound },
}]