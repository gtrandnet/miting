import Vue from 'vue'

export const Home = Vue.component('home-page', require('./Home.vue').default)
export const Rooms = Vue.component('rooms', require('./room/Rooms.vue').default)
export const ViewRoom = Vue.component('view-room', require('./room/ViewRoom.vue').default)
export const RoomTimeTable = Vue.component('room-time-table', require('./room/RoomTimeTable.vue').default)
export const UserBookingRooms = Vue.component('user-booking-rooms', require('./room/UserBookingRooms.vue').default)