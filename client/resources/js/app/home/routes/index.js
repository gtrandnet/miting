import { Home } from '../components'
import { Rooms } from '../components'
import { ViewRoom } from '../components'
import { UserBookingRooms } from '../components'

export default [{
        path: '/',
        components: { main: Home },
        name: 'home_page',
        children: [{
            path: '/rooms',
            components: { home: Rooms },
            name: 'rooms'
        }],
        meta: {
            needsAuth: true
        }
    },
    {
        path: '/show/:id/:title',
        components: { main: ViewRoom },
        name: 'view_room',
    },
    {
        path: '/booking-rooms',
        components: { main: UserBookingRooms },
        name: 'user_booking_rooms',
        meta: {
            needsAuth: true
        }
    }

]