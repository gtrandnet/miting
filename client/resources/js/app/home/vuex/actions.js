export const rooms = ({ dispatch }, { context }) => {
    return axios.get('/api/rooms').then((response) => {
        context.rooms = response.data
    }).catch((error) => {
        context.errors = error.response.data.errors
    })
}

export const getRooms = ({ dispatch }, { payload, context }) => {
    return axios.get(`/api/rooms?page=${payload.page}`).then((response) => {
        context.rooms = response.data.data
        context.meta = response.data.meta
    }).catch((error) => {
        context.error = error.response.data.error
    })
}

export const reserve = ({ dispatch }, { payload, context }) => {
    return axios.post('/api/reserve', payload, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
        .then((response) => {
            context.message = response.data
        }).catch((error) => {
            context.errors = error.response.data.errors
        })
}

export const timeTable = ({ dispatch }, { payload, context }) => {
    return axios.get(`/api/room-time-table/${payload.id}`).then((response) => {
        if (response.data.data.length == 0) {
            context.p = "Пока здесь пусто!"
        } else {
            context.time_tables = response.data.data
            context.p = "Расписание комнаты на сегодня"
        }
    }).catch((error) => {
        context.errors = error.response.data.errors
    })
}

export const getPartners = ({ dispatch }, { payload, context }) => {
    return axios.get(`/api/partners/${payload.id}`).then((response) => {
        if (response.data.length == 0) {
            context.p = "Пока здесь пусто!"
        } else {
            context.partners = response.data
        }
    }).catch((error) => {
        context.errors = error.response.data.errors
    })
}

export const getUserRooms = ({ dispatch }, { payload, context }) => {
    return axios.get(`api/my-booking-rooms`).then((response) => {
        context.rooms = response.data.data
    }).catch((error) => {
        context.errors = error.response.data.errors
    })
}
export const completeRoom = ({ dispatch }, { payload, context }) => {
    return axios.patch('api/complete/', payload).then((response) => {
        context.message = response.data
    }).catch((error) => {
        context.errors = error.response.data.errors
    })
}

export const extendRoom = ({ dispatch }, { payload, context }) => {
    return axios.patch('api/extend-room/', payload).then((response) => {
        context.message = response.data
    }).catch((error) => {
        context.errors = error.response.data.errors
    })
}