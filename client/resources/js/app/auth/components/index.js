import Vue from 'vue'

export const Login = Vue.component('login', require('./Login.vue').default)
export const Register = Vue.component('register', require('./Register.vue').default)
export const Profile = Vue.component('profile', require('./Profile.vue').default)