import { Login, Register, Profile } from '../components'

export default [{
        path: '/login',
        components: { main: Login },
        name: 'login',
        meta: {
            guest: true,
            needsAuth: false
        }
    },
    {
        path: '/register',
        components: { main: Register },
        name: 'register',
        meta: {
            guest: true,
            needsAuth: false
        }
    },
    {
        path: '/profile',
        components: { main: Profile },
        name: 'profile',
        meta: {
            guest: false,
            needsAuth: true
        }
    }
]