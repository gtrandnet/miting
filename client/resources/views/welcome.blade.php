<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>ECON</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href="css/app.css" rel="stylesheet">
        <!-- Plugin-CSS -->
        <link rel="stylesheet" href="{{asset('assets/css/normalize.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/bootstrap-min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/font-awesome-min.css')}}">
        <!-- Main-Stylesheets -->
        <link rel="stylesheet" href="{{asset('assets/css/helper.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/theme.css')}}">
        <link rel="stylesheet" href="{{asset('style.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/responsive.css')}}">
    </head>
    <body data-spy="scroll" data-target=".mainmenu-area">
        <div style="padding:0;margin:0" id="app">
            <app></app>
        </div>
        <script src="js/app.js"></script>
        <!--Vendor-JS-->
        <script src="{{asset('assets/js/vendor/jquery-1-12-4-min.js')}}"></script>
        <!--Plugin-JS-->
        <!--Main-active-JS-->
        <script src="{{asset('assets/js/main.js')}}"></script>
    </body>
</html>
