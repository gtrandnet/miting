<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>MitingRoom</title>
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href="{{asset('css/app.css')}}" rel="stylesheet">
        <link rel="shortcut icon" href="favicon.ico">
        <link rel="stylesheet" type="text/css" href="{{asset('fonts/font-awesome-4.3.0/css/font-awesome.min.css')}}" />
        <link rel="stylesheet" type="text/css" href="{{asset('css/demo.css')}}" />
        <link rel="stylesheet" type="text/css" href="{{asset('css/card.css')}}" />
        <link type="text/css" rel="stylesheet" href="{{asset('css/style.css')}}" />
        <link rel="stylesheet" href="{{asset('css/btn.css')}}">
        <style>
            body{
                font-size: 18px;
                background-color: #fff;
            }
            a{
               color: #fff;
            }
        </style>
    </head>
    <body data-spy="scroll" data-target=".mainmenu-area">
        <div id="app">
            <app-page></app-page>
        </div>
        <script src="{{asset('js/app.js')}}"></script>
    </body>
</html>
