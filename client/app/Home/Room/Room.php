<?php

namespace App\Home\Room;

use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    protected $fillable = ['title'];

    public function reservations()
    {
        return $this->hasMany(Reservation::class);
    }
}
