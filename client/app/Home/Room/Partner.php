<?php

namespace App\Home\Room;

use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
    protected $fillable = ['name'];

    public function reservation()
    {
        return $this->belongsTo(Reservation::class);
    }
}
