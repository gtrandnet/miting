<?php

namespace App\Home\Room;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $casts = [
        "check" => "boolean",
    ];

    protected $guarded  = [];
    
    public function room()
    {
        return $this->belongsTo(Room::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function partners()
    {
        return $this->hasMany(Partner::class);
    }

    public function setPartner($value)
    {
        $this->partners->attributes['name'] = $value;
    }
}
