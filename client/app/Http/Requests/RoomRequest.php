<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RoomRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function messages()
    {
        return [
            'start.required' => 'Не оставляйте поле начало даты пустым',
            'finish.required'  => 'Не оставляйте поле конец даты пустым',
        ];
    }

    public function rules()
    {
        return [
            'start' => 'required',
            'finish' => 'required',
            'file.*' => 'file|mimes:doc,pdf,docx,zip'
        ];
    }
}
