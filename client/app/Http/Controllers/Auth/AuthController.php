<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Tymon\JWTAuth\JWTAuth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\{RegisterFormRequest, LoginFormRequest};
use Tymon\JWTAuth\Exceptions\{JWTException};
use Auth;

class AuthController extends Controller
{
    protected $auth;

    public function __construct(JWTAuth $auth)
    {
        $this->auth = $auth;
    }

    public function login(LoginFormRequest $request)
    {
        try {
            if(!$token = $this->auth->attempt($request->only('email', 'password'))){
                return response()->json([
                    'errors' => [
                        'root' => 'Акаунт не существует!'
                    ]
                ], 401);
            }

        } catch (JWTException $e) {
            return response()->json([
                'errors' => [
                    'root' => 'Failed'
                ]
            ], $e->getStatusCode());
        }

        return response()->json([
            'data' => $request->user(),
            'meta' => [
                'token' => $token
            ]
        ], 200);

    }

    public function register(RegisterFormRequest $request)
    {
        $user = Auth::User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();

        return response()->json([
                'data' => "changed"
            ], 200);        
    }

    // public function register1()
    // {
    //     $user = User::create([
    //         'name' => 'gholib',
    //         'email' => "test.@alif.tj",
    //         'role' =>111,
    //         'password' => bcrypt(123456),
    //     ]);

    //     return response()->json([
    //             'data' => "changed"
    //         ], 200);        
    // }

    public function logout()
    {
        $this->auth->invalidate($this->auth->getToken());

        return response(null, 200);
    }

    public function user(Request $request)
    {
        return response()->json([
            'data' => $request->user()
        ]);
    }

    public function getUser()
    {
        $user = Auth::User();
        return response()->json([
            'data' => $user
        ]);
    }

}
