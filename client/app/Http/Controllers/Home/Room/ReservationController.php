<?php

namespace App\Http\Controllers\Home\Room;

use Illuminate\Http\Request;
use App\Http\Requests\RoomRequest;
use App\Http\Controllers\Controller;
use App\Transformers\TimeTableTransformer;
use App\Home\Room\Reservation;
use App\Home\Room\Partner;;
use Auth;
use Carbon\Carbon;
use DB;

class ReservationController extends Controller
{
    public function index($id)
    {
        $time_tables = Reservation::where([
                                            ['room_id', '=', $id],
                                            ['status', '=', 1],
                                            ['booking_start', '>', Carbon::today()],
                                            ['booking_start', '<', Carbon::tomorrow()],
                                        ])->get();
        
        return fractal()
                ->collection($time_tables)
                ->transformWith(new TimeTableTransformer)
                ->toArray();
    }

    public function store(RoomRequest $request)
    {
        $s = date($request->start);
        $f = date($request->finish);
        if($s >= $f || $s<= Carbon::now()){
            return response()->json("Неправильный формат даты!", 200);
        }
        $response = Reservation::where([
                                        ['status', '=', 1],
                                        ['room_id', '=', $request->room_id],
                                        ['booking_start', '>=', $s],
                                        ['booking_start', '<', $f],
                                    ])
                                    ->orWhere([
                                        ['status', '=', 1],
                                        ['room_id', '=', $request->room_id],
                                        ['booking_finish', '>', $s],
                                        ['booking_finish', '<=', $f],
                                    ])
                                    ->get();
        if(count($response) != 0){
            return response()->json("В это время комната занята! Выберите другую дату", 200);
        }
        $res = new Reservation;
        $res->user_id = Auth::User()->id;
        $res->room_id = $request->room_id;
        //$res->check = $request->check;
        $res->booking_start = $request->start;
        $res->booking_finish = $request->finish;
        if($request->file){
            $path = $request->file->store('files');
            $res->file = $path;
        }
        $res->save();
        $p = explode(',', $request->partners);
        if(count($p) > 0 && $p[0] != ""){
            foreach ($p as $value) {
                $partner = new Partner;
                $partner->name = $value;
                $res->partners()->save($partner);
            }
        }

        return response()->json("Вы успешно забронировали комнату!", 200);
    }

    public function getPartners(Reservation $res)
    {
        return response()->json($res->partners->toArray(), 200);
    }

    public function complete(Request $request)
    {
        $user = Auth::User();
        $res = Reservation::find($request->id);
        $res->status = 0;
        $user->reservations()->save($res);

        return response()->json("Бронирование успешно завершено!", 200);
    }

    public function extend(Request $request)
    {
        $s = date($request->start);
        $f = date($request->finish);
        $id = $request->id;
        if($s >= $f || $s<= Carbon::now()){
            return response()->json("Неправильный формат даты", 200);
        }
        $response = Reservation::where([
                                        ['id', '!=', $id],
                                        ['room_id', '=', $request->room_id],
                                        ['status', '=', 1],
                                        ['booking_start', '>=', $s],
                                        ['booking_start', '<', $f],
                                    ])
                                    ->orWhere([
                                        ['id', '!=', $id],
                                        ['room_id', '=', $request->room_id],
                                        ['status', '=', 1],
                                        ['booking_finish', '>', $s],
                                        ['booking_finish', '<=', $f],
                                    ])
                                    ->get();
        if(count($response) != 0){
            return "$id";
        }
        $user = Auth::User();
        $res = Reservation::find($request->id);
        $res->booking_finish = $request->finish;
        $user->reservations()->save($res);
        
        return response()->json("Вы успешно продлили!", 200);
    }
}
