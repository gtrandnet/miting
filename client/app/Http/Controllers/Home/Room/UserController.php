<?php

namespace App\Http\Controllers\Home\Room;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Tymon\JWTAuth\JWTAuth;
use Auth;
use Carbon\Carbon;

class UserController extends Controller
{
    public function getBookingRoom()
    {
        $user = User::find(Auth::User()->id);
        $data = $user->reservations->where('status', 1);
        foreach ($data as $value) {
            $response[] = [
                'id' => $value->id,
                'room_id' => $value->room_id,
                'title' => $value->room->title,
                'start' => $value->booking_start,
                'finish' => $value->booking_finish,
            ];
        }
        $response = isset($response) ? $response:[];
        return response()->json([
            'data' => $response
        ]);
    }

}
