<?php

namespace App\Http\Controllers\Home\Room;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\RoomRequest;
use App\Transformers\RoomTransformer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use App\Home\Room\Room;
use App\Home\Room\Reservation;
use Carbon\Carbon;
use DB;

class RoomController extends Controller
{
    public function index()
    {
        $data = Reservation::where([
                                    ['status', '=', 1],
                                    ['booking_finish', '<', Carbon::now()],
                                ])->get();
        foreach ($data as $value) {
            $value->status = 0;
            $value->save();
        }
        $rooms = Room::paginate(6);
        $roomsCollection = $rooms->getCollection();

        return fractal()
                ->collection($roomsCollection)
                ->transformWith(new RoomTransformer)
                ->paginateWith(new IlluminatePaginatorAdapter($rooms))
                ->toArray();
    }

}
