<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Home\Room\Room;
use Carbon\Carbon;

class RoomTransformer extends TransformerAbstract
{
    public function transform(Room $room)
    {
        $count = $room->reservations()->where([
                                            ['status', '=', 1],
                                            ['booking_start', '>', Carbon::today()],
                                            ['booking_start', '<', Carbon::tomorrow()]
                                        ])
                                        ->get();
        $status = count($count)>0 ? 0:1;
        return [
            'id' => $room->id,
            'title' => $room->title,
            'status' => $status,
        ];
    }
}