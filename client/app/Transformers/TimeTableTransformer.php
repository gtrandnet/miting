<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Home\Room\Reservation;
use Carbon\Carbon;

class TimeTableTransformer extends TransformerAbstract
{
    public function transform(Reservation $res)
    {
        return [
            'id' => $res->id,
            'room_id' => $res->room_id,
            'title' => $res->room->title,
            'start' => Carbon::parse($res->booking_start)->diffForHumans(),
            'finish' => Carbon::parse($res->booking_finish)->diffForHumans(),
        ];
    }
}