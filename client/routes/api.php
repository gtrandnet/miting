<?php

use Illuminate\Http\Request;

// Route::get('/register1', 'Auth\AuthController@register1');
Route::post('/login', 'Auth\AuthController@login');
Route::post('/logout', 'Auth\AuthController@logout');

Route::group(['middleware' => 'jwt.auth'], function(){
    Route::post('/register', 'Auth\AuthController@register');
    Route::get('/me', 'Auth\AuthController@user');
    Route::get('/user', 'Auth\AuthController@getUser');
    Route::get('/my-booking-rooms', 'Home\Room\UserController@getBookingRoom');
    Route::get('/rooms', 'Home\Room\RoomController@index');
    Route::post('/reserve', 'Home\Room\ReservationController@store');
    Route::get('/room-time-table/{id}', 'Home\Room\ReservationController@index');
    Route::get('/partners/{res}', 'Home\Room\ReservationController@getPartners');
    Route::patch('/complete', 'Home\Room\ReservationController@complete');
    Route::patch('/extend-room', 'Home\Room\ReservationController@extend');
});



